// // Dado el siguiente HTML:

// const { brotliDecompressSync } = require("zlib");

// // 1.1 Usa querySelector para mostrar por consola el botón con la clase .showme

// var claseShowMe=document.querySelector('.showme');
// console.log(claseShowMe)

// // 1.2 Usa querySelector para mostrar por consola el h1 con el id #pillado

// var idPillado = document.querySelector('#pillado');
// console.log(idPillado);

// // 1.3 Usa querySelector para mostrar por consola todos los p

// var todosLosP = document.querySelectorAll('p');
// console.log(todosLosP)

// // 1.4 Usa querySelector para mostrar por consola todos los elementos con la clase.pokemon

// var clasePokemon = document.querySelectorAll('.pokemon');
// console.log(clasePokemon);

// // 1.5 Usa querySelector para mostrar por consola todos los elementos con el atributo
// // data-function="testMe".

// var atributoData= document.querySelectorAll('[data-function="testMe"]');
// console.log(atributoData);

// // 1.6 Usa querySelector para mostrar por consola el 3 personaje con el atributo
// // data-function="testMe".

// var atributoData= document.querySelectorAll('[data-function="testMe"]');
// console.log(atributoData[2])


window.onload=()=>{

// Iteraccion#2

// 2.1 Inserta dinamicamente en un html un div vacio con javascript.

    // const divVacio = document.createElement('div');

    // const container = document.getElementById('insert-here');

    // container.appendChild(divVacio);

// 2.2 Inserta dinamicamente en un html un div que contenga un p con javascript.

    // const divVacio = document.createElement('div');
    // const pVacio= document.createElement('p');

    // divVacio.appendChild(pVacio);

    // const container = document.querySelector('body');

    // container.appendChild(divVacio);


// 2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.

    // function createSixElements () {

    // const container = document.querySelector('body');
    // const divVacio = document.createElement('div');
    // container.appendChild(divVacio);

    // for (let i = 0; i < 6; i++) {
    //     const pVacio= document.createElement('p');
    //     divVacio.appendChild(pVacio);
    // }
    // }

    // createSixElements();

// 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.

    // const divVacio = document.createElement('div');
    // const container = document.querySelector('body');
    // const p = document.createElement('p');

    // p.textContent='Soy dinámico';

    // container.appendChild(divVacio);
    // divVacio.appendChild(p);


// 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.

    // const h2 = document.querySelector('.fn-insert-here');
    // h2.textContent="Wubba Lubba dub dub";

// 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
// const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

    // const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

    // function crearList () {

    //     const ul = document.createElement('ul');
    //     const body = document.querySelector('body');
    //     body.appendChild(ul);

    //     apps.forEach(app => {
    //         var element = document.createElement('li');
    //         element.textContent=app;

    //         ul.appendChild(element);
    //     });


    // }

    // crearList()

// 2.7 Elimina todos los nodos que tengan la clase .fn-remove-me

    // var eliminated = document.querySelectorAll('.fn-remove-me');
    // eliminated.forEach(element => element.remove());


// 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div.
// 	Recuerda que no solo puedes insertar elementos con .appendChild.

    // var pEnMedio = document.createElement('p');
    // pEnMedio.textContent='Voy en medio!'
    // const body = document.querySelector('body')

    // const divsContainer = document.querySelectorAll('div');
    // const divContainer = divsContainer[1];

    // body.insertBefore(pEnMedio, divContainer);


// 2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here


//     const divsParaMeter = document.querySelectorAll('div.fn-insert-here');

//     divsParaMeter.forEach(element => {
//         const p = document.createElement('p');
//         p.textContent="Voy dentro!";

//         element.appendChild(p)
//     });
//
}